export interface GeolocationCoordinates {
  latitude: number;
  longitude: number;
  timestamp?: number;
}
