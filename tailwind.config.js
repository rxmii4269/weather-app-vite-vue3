module.exports = {
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        blue: {
          950: '#1E213A',
          960: '#100E1D',
        },
        yellow: {
          350: '#FFEC65',
        },
        gray: {
          350: '#E7E7EB',
          650: '#6E707A',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
