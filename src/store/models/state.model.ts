/* eslint-disable @typescript-eslint/no-explicit-any */
// declare your own store states
export interface State {
  ClearImg: string;
  Cloudbg: string;
  HailImg: string;
  HeavyCloudImg: string;
  HeavyRainImg: string;
  LightCloudImg: string;
  LightRainImg: string;
  ShowerImg: string;
  SleetImg: string;
  SnowImg: string;
  ThunderStormImg: string;
  locationInfo: Record<string, any>;
  weatherInfo: Record<string, any>;
  defaultTemp: string;
  isCelcius: boolean;
  isFahrenheit: boolean;
  currentWeather: string;
}
