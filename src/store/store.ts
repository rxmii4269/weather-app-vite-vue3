import { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import { State } from "./models/state.model";
import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";

export const key: InjectionKey<Store<State>> = Symbol("");

export const store = createStore<State>({
  state: {
    ClearImg: "Clear.png",
    Cloudbg: "Cloud-background.png",
    HailImg: "Hail.png",
    HeavyCloudImg: "HeavyCloud.png",
    HeavyRainImg: "HeavyRain.png",
    LightCloudImg: "LightCloud.png",
    LightRainImg: "LightRain.png",
    ShowerImg: "Shower.png",
    SleetImg: "Sleet.png",
    SnowImg: "Snow.png",
    ThunderStormImg: "ThunderStorm.png",
    locationInfo: {},
    weatherInfo: {},
    defaultTemp: "C",
    isCelcius: true,
    isFahrenheit: false,
    currentWeather: "",
  },
  getters,
  mutations,
  actions,
  modules: {},
});

export function useStore(): Store<State> {
  return baseUseStore(key);
}
