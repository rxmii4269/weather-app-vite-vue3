/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable max-len */
import { GetterTree } from "vuex";
import { State } from "./models/state.model";
import {
  celciusToFahrenheit,
  fahrenheitToCelcius,
  unixToDate,
  metersToMiles,
} from "../../helpers/helpers";

const getters: GetterTree<State, State> = {
  getCurrentWeatherIcon: (state): string =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.weather[0].icon
      : "Clear.png",
  tempC: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? fahrenheitToCelcius(state.weatherInfo.current.temp)
      : 25,
  tempF: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.temp
      : celciusToFahrenheit(25),
  currentDate: (state): string =>
    Object.keys(state.weatherInfo).length > 0
      ? unixToDate(state.weatherInfo.current.dt)
      : "Fri, 5 Jun",
  currentDesc: (state): string =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.weather[0].description
      : "Shower",
  currentLocation: (state): string =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.location
      : "Helsinki",
  getCurrentWindDeg: (state): [string, number] | number =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.wind_deg
      : ["N", 0 + -44],
  getCurrentWindSpd: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.wind_speed
      : 7,
  getCurrentHumidity: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.humidity
      : 84,
  getCurrentVisibility: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? metersToMiles(state.weatherInfo.current.visibility)
      : 6.4,
  getCurrentAirPressure: (state): number =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.current.pressure
      : 998,
  getFiveDayForecast: (state): string | Record<string, any>[] =>
    Object.keys(state.weatherInfo).length > 0
      ? state.weatherInfo.daily.slice(1, 6)
      : [
          {
            dt: 1625879166,
            temp: { min: 51.8, max: 60.8 },
            weather: [{ icon: "Clear.png" }],
          },
          {
            dt: 1625879166,
            temp: { min: 51.8, max: 60.8 },
            weather: [{ icon: "Clear.png" }],
          },
          {
            dt: 1625879166,
            temp: { min: 51.8, max: 60.8 },
            weather: [{ icon: "Clear.png" }],
          },
          {
            dt: 1625879166,
            temp: { min: 51.8, max: 60.8 },
            weather: [{ icon: "Clear.png" }],
          },
          {
            dt: 1625879166,
            temp: { min: 51.8, max: 60.8 },
            weather: [{ icon: "Clear.png" }],
          },
        ],
  convertCelciusToFahrenheit:
    (state) =>
    (temp: number): number =>
      celciusToFahrenheit(temp),
  convertFahrenheitToCelcius:
    (state) =>
    (temp: number): number =>
      fahrenheitToCelcius(temp),
};

export default getters;
