/* eslint-disable max-len */
import { MutationTree } from "vuex";
import { State } from "./models/state.model";

const mutations: MutationTree<State> = {
  isClear: (state: { currentWeather: string; ClearImg: string }): void => {
    state.currentWeather = state.ClearImg;
  },
  isHail: (state: { currentWeather: string; HailImg: string }): void => {
    state.currentWeather = state.HailImg;
  },
  isHeavyCloud: (state: {
    currentWeather: string;
    HeavyCloudImg: string;
  }): void => {
    state.currentWeather = state.HeavyCloudImg;
  },
  isHeavyRain: (state: {
    currentWeather: string;
    HeavyRainImg: string;
  }): void => {
    state.currentWeather = state.HeavyRainImg;
  },
  isLightCloud: (state: {
    currentWeather: string;
    LightCloudImg: string;
  }): void => {
    state.currentWeather = state.LightCloudImg;
  },
  isLightRain: (state: {
    currentWeather: string;
    LightRainImg: string;
  }): void => {
    state.currentWeather = state.LightRainImg;
  },
  isShower: (state: { currentWeather: string; ShowerImg: string }): void => {
    state.currentWeather = state.ShowerImg;
  },
  isSleet: (state: { currentWeather: string; SleetImg: string }): void => {
    state.currentWeather = state.SleetImg;
  },
  isSnowing: (state: { currentWeather: string; SnowImg: string }): void => {
    state.currentWeather = state.SnowImg;
  },
  isThunderStorm: (state: {
    currentWeather: string;
    ThunderStormImg: string;
  }): void => {
    state.currentWeather = state.ThunderStormImg;
  },
  setLocationInfo: (state, locationInfo): void => {
    state.locationInfo = locationInfo;
  },
  setWeatherInfo: (state, weatherInfo): void => {
    state.weatherInfo = weatherInfo;
  },
  resetLocationInfo: (state): void => {
    state.locationInfo = {};
  },
  setToCelcius: (state: {
    defaultTemp: string;
    isCelcius: boolean;
    isFahrenheit: boolean;
  }): void => {
    state.defaultTemp = "C";
    state.isCelcius = true;
    state.isFahrenheit = false;
  },
  setToFahrenheit: (state: {
    defaultTemp: string;
    isFahrenheit: boolean;
    isCelcius: boolean;
  }): void => {
    state.defaultTemp = "F";
    state.isFahrenheit = true;
    state.isCelcius = false;
  },
};

export default mutations;
