/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { AxiosResponse } from "axios";
import { ActionTree } from "vuex";
import { weatherToIcon, degreesToWindDirection } from "../../helpers/helpers";
import { State } from "./models/state.model";
import {
  getLocation,
  reverseGeocoding,
} from "../../services/geocoding.service";
import getForecast from "../../services/weather.service";

const actions: ActionTree<State, State> = {
  getCurrentForecast: async (
    { commit, dispatch },
    {
      lat,
      lng,
      formattedaddress,
    }: { lat: number; lng: number; formattedaddress: string }
  ): Promise<AxiosResponse<any>> => {
    const response = await getForecast(lat, lng);

    if (typeof formattedaddress === "undefined") {
      const latlng = { lat, lng };
      const reslt = await dispatch("getReverseGeocoding", latlng);
      // eslint-disable-next-line no-param-reassign
      formattedaddress = reslt[0].formatted_address;
    }
    if (response.status === 200) {
      const icon = response.data.current.weather[0].id;
      response.data.current.weather[0].icon = weatherToIcon(icon);
      response.data.location = formattedaddress;
      response.data.current.wind_deg = degreesToWindDirection(
        response.data.current.wind_deg
      );
      response.data.daily.forEach((d: Record<string, any>) => {
        // eslint-disable-next-line no-param-reassign
        d.weather[0].icon = weatherToIcon(d.weather[0].id);
      });
      commit("setWeatherInfo", response.data);
    }
    return response;
  },
  getCurrentLocation: async ({ commit }, address: string): Promise<void> => {
    const response = await getLocation(address);
    if (response.status === 200) {
      commit("setLocationInfo", response.data.results);
    }
  },
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  getReverseGeocoding: async (
    { commit },
    { lat, lng }: { lat: number; lng: number }
  ): Promise<string[]> => {
    const response = await reverseGeocoding(lat, lng);
    return response.data.results;
  },
};

export default actions;
