import axios, { AxiosResponse } from "axios";

const { VITE_WEATHER_API_KEY, VITE_WEATHER_API } = import.meta.env;
const baseURL = VITE_WEATHER_API;

const Axios = axios.create({
  baseURL,
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export default async function getForecast(
  lat: number,
  lng: number
): Promise<AxiosResponse<any>> {
  const response = await Axios.get(
    `onecall?lat=${lat}&lon=${lng}&exclude=minutely,hourly,alerts&units=imperial&appid=${VITE_WEATHER_API_KEY}`
  );
  return response;
}
