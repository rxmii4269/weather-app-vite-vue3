/* eslint-disable @typescript-eslint/no-explicit-any */
import axios, { AxiosResponse } from "axios";
const { VITE_API_KEY, VITE_GEOCODING_API } = import.meta.env;

const baseURL = VITE_GEOCODING_API;
const Axios = axios.create({
  baseURL,
  headers: {
    "X-RapidAPI-Key": VITE_API_KEY,
    "X-RapidAPI-Host": baseURL?.split("https://")[1],
  },
});

export async function getLocation(
  address: string
): Promise<AxiosResponse<any>> {
  const response = await Axios.get(
    `geocode/json?address=${address}&language=en`
  );
  return response;
}

export async function reverseGeocoding(
  lat: number,
  lng: number
): Promise<AxiosResponse<any>> {
  const response = await Axios.get(
    `geocode/json?latlng=${lat},${lng}&language=en&result_type=locality`
  );
  return response;
}
